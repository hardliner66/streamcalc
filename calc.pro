#-------------------------------------------------
#
# Project created by QtCreator 2015-06-12T10:10:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = calc
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    task.cpp \
    calc.cpp \
    operation.cpp

HEADERS += \
    task.h \
    calc.h \
    operation.h
