#ifndef CALC_H
#define CALC_H

#include <QObject>
#include <QStack>
#include <QTextStream>
#include "operation.h"

class Calculator : public QObject
{
    Q_OBJECT
private:
    enum Operators
    {
        ADD = (int)'+',
        SUB = (int)'-',
        MUL = (int)'*',
        DIV = (int)'/',
        NL = (int)'\n'
    };

    QStack<double> m_numbers;
    int m_lastoperator = ADD;

    inline bool isOperator(char token);
    inline bool isNumber(char token);
public:
    explicit Calculator(QObject *parent = 0);
    ~Calculator();

    void reset();
    double calc(QTextStream *input);
signals:

public slots:
};

#endif // CALC_H
