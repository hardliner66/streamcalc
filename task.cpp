#include "task.h"

Task::Task(QObject *parent) : QObject(parent)
{

}

Task::~Task()
{

}

void Task::run()
{
    // Do processing here
    Calculator c;
    c.reset();
    QTextStream stream(stdin);
    QString sum = QString::number(c.calc(&stream));
//    QTextStream outp(stdout);
    std::cout.write(sum.toStdString().c_str(), sum.length());
    emit finished();
}
