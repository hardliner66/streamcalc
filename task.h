#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QTextStream>
#include <iostream>
#include "calc.h"

class Task : public QObject
{
    Q_OBJECT
public:
    explicit Task(QObject *parent = 0);
    ~Task();

public slots:
    void run();

signals:
    void finished();
};

#endif // TASK_H
