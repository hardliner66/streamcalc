#ifndef OPERATION_H
#define OPERATION_H

#include <QObject>
#include <QStack>

class OperationStack
{
private:
    enum Operators
    {
        ADD = (int)'+',
        SUB = (int)'-',
        MUL = (int)'*',
        DIV = (int)'/',
        NL = (int)'\n'
    };
    QStack<OperationStack> m_operations;
    char m_operator = ADD;
    double m_value = 0;
public:
    OperationStack();
    OperationStack(char op, double value);
    ~OperationStack();

    void add(char op, double value);
    void add(char op, OperationStack operations);
};

#endif // OPERATION_H
