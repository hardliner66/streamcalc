#include "calc.h"
#include <iostream>

Calculator::Calculator(QObject *parent) : QObject(parent)
{

}

Calculator::~Calculator()
{

}

inline bool Calculator::isNumber(char token)
{
    return (token >= 48) && (token <= 57);
}

inline bool Calculator::isOperator(char token)
{
    return (token == ADD) || (token == SUB) || (token == DIV) || (token == MUL) || (token == NL);
}

void Calculator::reset()
{
    m_numbers.clear();
}

double Calculator::calc(QTextStream *input)
{
//    QTextStream stream(stdin);
    QString token = QString("");
    QString line;
    int pendingMinuses = 0;
    bool running = true;
    do {
        line = input->read(1);
        char current = line[0].toLatin1();
        std::cout.put(current);
        if (line.isNull())
            running = false;
        if ((int)current == NL)
            running = false;
        if (isNumber(current))
        {
            token.append(current);
        } else {
            if (isOperator(current)) {
                if (token != "") {
                    double num = token.toDouble();
                    for (int i = 0; i < pendingMinuses; i++)
                        num *= -1;
                    switch (m_lastoperator) {
                    case ADD:
                        m_numbers.push(num);
                        break;
                    case SUB:
                        m_numbers.push(-num);
                        break;
                    case MUL:
                        m_numbers.push(m_numbers.pop() * num);
                        break;
                    case DIV:
                        m_numbers.push(m_numbers.pop() / num);
                        break;
                    default:
                        break;
                    }
                    m_lastoperator = current;
                    token = QString("");
                } else {
                    if ((m_lastoperator == ADD) && (current == SUB)) {
                        m_lastoperator = SUB;
                    }
                    else if ((m_lastoperator == SUB) && (current == SUB)) {
                        m_lastoperator = ADD;
                    } else if ((m_lastoperator == MUL || m_lastoperator == DIV) && current == SUB){
                        pendingMinuses++;
                    }
                }
            }
        }
    } while(running);
    double sum = 0;
    while (!m_numbers.isEmpty()) {
        sum += m_numbers.pop();
    }
    return sum;
}
